use slint::{Model, ModelRc};
slint::include_modules!();

fn generate_time_table(selected: ModelRc<ModelRc<bool>>) {
	let v = selected.iter().map(|x| x.iter().collect()).collect::<Vec<Vec<bool>>>();
	println!("{:?}", v);
}

fn main() {
	let main_window = Example::new();
	main_window.on_generate_time_table(generate_time_table);
	main_window.run();
}
